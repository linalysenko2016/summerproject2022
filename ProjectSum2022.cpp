#include "ProjectSummer2022.h"

void print_vec(int n, int m, vector<vector <int>> a) {
	for (int i = 0; i < n; i++) {   
		for (int j = 0; j < m; j++) 
		{
			cout << setw(5) << a[i][j] << ' ';
		}
		cout << endl;
	}
}

VPInt hungarian(const VVInt& matrix, int& fin_bud) {
	
	// ������� �������
	int height = matrix.size(), width = matrix[0].size();
	
	// ��������, ���������� �� ����� (u) � �������� (v)
	VInt u(height, 0), v(width, 0);
	
	// ������ ���������� ������ � ������ �������
	VInt markIndices(width, -1);

	// ����� ��������� ������ ������� ���� �� ������
	for (int i = 0; i < height; i++) {
		VInt links(width, -1);    
		VInt mins(width, inf);  
		VInt visited(width, 0);
		
		// ���������� �������� (�������� "������������ �������" �� ������� ���������)
		int markedI = i, markedJ = -1, j; //j - �������������������� int ����������
		
		while (markedI != -1) {
			// ������� ���������� � ��������� � ���������� ������� ������������ ��������
			// ������ �������� � j ������ ������������� ������� � ����� ��������� �� ���
			j = -1;
			for (int j1 = 0; j1 < width; j1++)
				if (!visited[j1]) {
					if (matrix[markedI][j1] - u[markedI] - v[j1] < mins[j1]) {
						mins[j1] = matrix[markedI][j1] - u[markedI] - v[j1];
						links[j1] = markedJ;
				    }
					if (j == -1 || mins[j1] < mins[j])
						j = j1;
					
				}

			// ������ ��� ���������� ������� � ��������� (markIndices[links[j]], j)
			// ���������� ����������� �� �������� � ��������� ���, ����� �� ���������
			int delta = mins[j];
			for (int j1 = 0; j1 < width; j1++)
				if (visited[j1]) {
					u[markIndices[j1]] += delta;
					v[j1] -= delta;
				}
				else {
					mins[j1] -= delta;
				}
			u[i] += delta;

			// ���� �������� �� ��������� - �������� � ��������� ��������
			visited[j] = 1;
			markedJ = j;
			markedI = markIndices[j];
		}
		
		// ������� �� ��������� ������������ ������� ������, ������ ������� �
		// ���������� ������ � �������� ������� �� ������������
		//cout << links.size();
		for (; links[j] != -1; j = links[j]) {
			markIndices[j] = markIndices[links[j]];
		}
		markIndices[j] = i;
		
	}

	// ������ ��������� � ������������ �����
	VPInt result;
	int sum = 0;
	for (int j = 0; j < width; j++) 
		if (markIndices[j] != -1) 
			result.push_back(PInt(markIndices[j], j));
			
	for (auto i = 0; i < result.size(); i++) 
		fin_bud += matrix[result[i].first][result[i].second];
	
	return result;
}