
#pragma once
#define inf 2147483647
#include <Windows.h>
#include <iostream>
#include <algorithm>
#include <vector>
#include <random>
#include <limits>
#include <iomanip>

using namespace std;

typedef pair<int, int> PInt;
typedef vector<int> VInt;
typedef vector<VInt> VVInt;
typedef vector<PInt> VPInt;

void print_vec(int n, int m, vector<vector <int>> a);
VPInt hungarian(const VVInt& matrix, int& fin_bud);





