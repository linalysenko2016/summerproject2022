﻿#include "ProjectSummer2022.h"

int n, m;  //n - works, m = workers
int M, N;
int budget,  fin_bud = 0;

int main() {
	N = 0;
	M = 50;
	budget = 2 * M;
	cout << "Enter the numbers of works >> ";
	cin >> m;
	cout << "Enter the numbers of workers >> ";
	cin >> n;
	
	vector <vector <int> > value(n, vector <int>(m));   // Матрица эффективности a[разраб][задача]
	//Creating vector
	if (n < m) {
        value.resize(m, vector<int>(m));
		for (int i = 0; i < m; i++)     // Цикл, который идёт по строкам
			for (int j = 0; j < m; j++) // Цикл, который идёт по элементам
			{
				if (i < n) {
				int val = N + std::rand() % (M - N + 1);
				value[i][j] = val;
				}
				else {
					value[i][j] = 0;
				}
			}
		print_vec(m, m, value);
		
	}
	else if (n == m) {
		for (int i = 0; i < n; i++)     
			for (int j = 0; j < m; j++) 
			{
				int val = N + std::rand() % (M - N + 1);
					value[i][j] = val;
			}
		print_vec(m, m, value);
	}
    else {
		
		for (int i = 0; i < n; i++) {
			value[i].resize(n);
			for (int j = 0; j < n ; j++) 
			{
				if (j < m) {
					int val = N + std::rand() % (M - N + 1);
					value[i][j] = val;
				}
				else
					value[i][j] = 0;
			}
		}
		print_vec(n, n, value);
    }

	cout << endl;
	VPInt res = hungarian(value, fin_bud);
	if (fin_bud <= budget) {
		cout << "Min cost = " << fin_bud << endl;
		cout << "Budget is enough " << endl;
	}
	else
		cout << "Budget isn't enough" << endl;
	cout << endl;
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	for (int i = 0; i < n; i++) {   
		for (int j = 0; j < m; j++) { 
			SetConsoleTextAttribute(hStdOut, 0x0f | 0x0f);
			for (int k = 0; k < res.size(); k++) {
                if (i == res[k].first && j == res[k].second ) 
					SetConsoleTextAttribute(hStdOut, FOREGROUND_BLUE | BACKGROUND_GREEN| BACKGROUND_RED);
				
			}
			cout << setw(5) << value[i][j] << ' ';
		}
		cout << endl;
	}
	SetConsoleTextAttribute(hStdOut, 0x0f | 0x0f);
	return 0;
}
